; TEXT
%macro Bios_print_char 1
    mov si, %1
    lodsb
    mov ah, 0x0E
    int 0x10
%endmacro

%macro Bios_print_str 1 
ch_loop:
   lodsb
   or al, al
   jz done
   mov ah, 0x0E
   int 0x10
   jmp ch_loop
done:
%endmacro
 
[ORG 0x7c00]
   xor ax, ax
   mov ds, ax
 
   Bios_print_char msg
   Bios_print_str msg
hang:
   jmp hang

; DATA 
   msg db 'Hello World', 13, 10, 0 
   times 510-($-$$) db 0
   db 0x55
   db 0xAA
