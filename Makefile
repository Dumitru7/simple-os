all: 
	@echo  This Makefile doesnt make all rules. Run it by steps.  
run: compile mount
	qemu-system-x86_64 -drive format=raw,file=boot.img 
compile:
	nasm src/boot.asm -f bin -o boot.bin
	#nasm -f elf64 src/boot.asm -o src/boot.o
	#gcc -c src/test.c -o src/test.o
	#gcc -no-pie src/test.o src/boot.o -o boot.bin
mount:
	dd if=/dev/zero of=boot.img bs=1024 count=1440
	dd if=boot.bin of=boot.img conv=notrunc
clean:
	@rm boot.*
	@echo Clear!!!
